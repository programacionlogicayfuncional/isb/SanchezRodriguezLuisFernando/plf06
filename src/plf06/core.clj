;á = 160 S/N =
;é = 130 S/N =
;í = 161 S/N =
;ó = 162 S/N =
;ú = 163 S/N =
;Á= 181 S/N = 65
;É = 144 S/N =
;Í = 214 S/N =
;Ó = 224 S/N =
;Ú = 233 S/N =
(ns plf06.core
  (:gen-class))

;(get [1] 0)
;(get [1 2 3 4 5] (+ (.indexOf [1 2 3] 1) 1))
;util para obtener el caracter en la posicion indicada
(def mini [\a \á \b \c \d \e \é \f \g \h \i \í \j \k \l \m \n \ñ \o \ó \p \q \r \s \t \u \ú \v \w \x \y \z])
(def may [\A \Á \B \C \D \E \É \F \G \H \I \Í \J \K \L \M \N \Ñ \O \Ó \P \Q \R \S \T \U \Ú \V \X \Y \Z])
(def simbol [1 2 3 4 \! \" \# \$ \% \& \' \( \) \* \+ \, \- \. \/ \: \; \< \= \> \? \@ \[ \\ \] \^ \_ \{ \| \} \~ \5 \6 \7 \8 \9])


(defn list-contains? [coll value]
  (let [s (seq coll)]
    (if s
      (if (= (first s) value) value (recur (rest s) value))
      false)))

(defn comp
  [s]
  (cond
      (== (int (list-contains? may s)) (int s)) (.indexOf may s)
    (or (== (int (list-contains? simbol s)) (int s))) (.indexOf simbol s)
    (or (== (int (list-contains? mini s)) (int s))) (.indexOf mini s)

    :else false)  
  )
(defn comp1
  [s]
  (if 
   (== (int (list-contains? may s)) (int s)) (.indexOf may s) (false)
   )
  (if
   (== (int (list-contains? simbol s)) (int s)) (.indexOf may s) (false))
  )


(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))
